package com.ndkj.blog.controller;

import com.alibaba.fastjson.JSON;
import com.ndkj.blog.constant.FaceConstant;
import com.ndkj.blog.service.impl.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @program: Blog
 * @description: LoginController
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
@Controller
@Slf4j
@RequestMapping("/login")
@SessionAttributes(value = "loginUser")
public class FaceLoginController {

    @Autowired
    LoginService loginService = null;

    @RequestMapping("/scan-face")
    public String getface() {
        return "admin/getface";
    }

    @RequestMapping("/search-face")
    @ResponseBody
    public String searchface(@RequestBody @RequestParam(name = "imagebast64") StringBuffer imagebast64
            , Model model, HttpServletRequest request) throws IOException {
        return loginService.searchFace(imagebast64, model, request);
    }

    @RequestMapping("/face-success")
    public String faceLoginSucceed(HttpSession session, Model model) {
        return loginService.faceLoginSessionCheck(session, model);
    }

}
