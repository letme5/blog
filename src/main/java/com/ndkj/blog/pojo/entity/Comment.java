package com.ndkj.blog.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @program: SpringBoot
 * @description:
 * @author: liuyong
 * @create: 2021-08-01 17:56
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    public Integer comId;
    private Date comTime;
    private String comment;
    private String comName;
    private Integer blogId;
    private String avatarPath;
}
