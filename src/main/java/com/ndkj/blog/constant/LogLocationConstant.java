package com.ndkj.blog.constant;

/**
 * @program: Blog
 * @description: LogLocationConstant
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
public interface LogLocationConstant {

    /**
     * 邮件发送时附件日志的linux位置
     */
    String LOG_LOCATION = "/root/logs/log.out";

    String MAIL_LOG_FILE_NAME = "running_log.txt";

}
