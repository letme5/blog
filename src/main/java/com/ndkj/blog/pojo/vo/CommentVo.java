package com.ndkj.blog.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: Personal-Blog
 * @description: admin/commentVo
 * @author: liuyong
 * @create: 2022-04-19 11:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentVo implements Serializable {
    /**
     * 定义为public解决springboot返回ModelAndView时获取不到该属性的bug
     */
    public Integer comId;
    private Date comTime;
    private String comment;
    private String comName;
    private String blogTitle;
    private String avatarPath;
}
