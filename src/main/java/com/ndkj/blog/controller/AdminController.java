package com.ndkj.blog.controller;

import com.ndkj.blog.pojo.entity.Blog;
import com.ndkj.blog.pojo.entity.IpLog;
import com.ndkj.blog.service.impl.AdminServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @program: SpringBoot
 * @description: 后台管理控制
 * @author: liuyong
 * @create: 2021-07-28 17:11
 **/
@Controller
@Slf4j
public class AdminController {

    @Autowired
    private AdminServiceImpl adminService;

    @GetMapping("/admin")
    public String toAdmin() {
        return "admin/login";
    }

    @PostMapping("/login")
    public String login(@RequestParam("username") String userName, @RequestParam("password") String userPwd,
                        HttpSession session, Model model, HttpServletRequest request) throws ServletException {
        return adminService.loginControl(userName, userPwd, session, model, request);
    }

    @GetMapping("/admin/adminIndex")
    public String adminIndex(String username, Model model) {
        return adminService.adminIndex(username, model);
    }

    @GetMapping("/admin/comment")
    public String comment(Model model) {
        return adminService.CommentList(model);
    }

    @GetMapping("/admin/flink")
    public String flink() {
        return "admin/flink";
    }

    @GetMapping("/admin/manageUser")
    public String manageUser() {
        return "admin/manage-user";
    }

    @GetMapping("/admin/notice")
    public String notice() {
        return "admin/notice";
    }


    @GetMapping("/admin/adminArticle")
    public String adminArticle(Model model) {
        return adminService.ArticleList(model);
    }

    @GetMapping("/admin/addArticle")
    public String addArticle(Model model) {
        model.addAttribute("args", adminService.queryArgsList());
        return "admin/add-article";
    }

    /**
     * 接收新建文章的form表单提交
     */
    @PostMapping("/admin/add")
    public String add(@RequestParam("title") String title, @RequestParam("args") String args,
                      @RequestParam("content") String content, @RequestParam("readNum") Integer readNum) {
        adminService.addBlog(new Blog(1, new Date(), readNum, title, content, args, 1));
        return "redirect:/admin/adminArticle";
    }

    /**
     * 跳转至修改文章信息
     */
    @GetMapping("/admin/update/{blogId}")
    public String updateBlog(@PathVariable("blogId") Integer blogId, Model model) {
        if (adminService.isBlogExist(blogId) == 1) {
            model.addAttribute("args", adminService.queryArgsList());
            model.addAttribute("targetBlog", adminService.queryBlogById(blogId));
            return "admin/update-article";
        } else {
            return "redirect:/error/404";
        }
    }

    /**
     * 接收提交的修改文章信息，修改信息
     */
    @PostMapping("/admin/updateBlog/{blogId}")
    public String updateBlogInfo(@RequestParam("title") String title, @RequestParam("content") String content,
                                 @RequestParam("args") String args, @RequestParam("putTime") Date putTime,
                                 @PathVariable("blogId") Integer blogId, @RequestParam("readNum") Integer readNum,
                                 Model model) {
        if (adminService.isBlogExist(blogId) == 1) {
            adminService.updateBlog(new Blog(blogId, putTime, readNum, title, content, args, 1));
            return "redirect:/admin/adminArticle";
        } else {
            return "redirect:/error/404";
        }
    }

    /**
     * 删除博客
     */
    @GetMapping("/admin/delete/{blogId}")
    public String deleteBlog(@PathVariable("blogId") Integer blogId, Model model) {
        if (adminService.isBlogExist(blogId) == 1) {
            adminService.deleteBlogById(blogId);
            return "redirect:/admin/adminArticle";
        } else {
            return "redirect:/error/404";
        }
    }

    /**
     * 根据id删除评论
     */
    @GetMapping("/admin/deleteComment/{comId}")
    public String deleteComment(@PathVariable("comId") Integer comId, Model model) {
        adminService.deleteCommentById(comId);
        return "redirect:/admin/comment";
    }

    /**
     * 访问记录页,支持最近300条访问记录查看
     */
    @GetMapping("/admin/accessLog")
    public String accessLog(Model model) {
        List<IpLog> ipLogs = adminService.queryLogsNum();
        model.addAttribute("ipLogs", ipLogs);
        return "admin/accessLog";
    }

    /**
     * 删除访问记录
     */
    @GetMapping("/admin/deleteLog/{uId}")
    public String deleteLogById(@PathVariable("uId") Integer uId) {
        adminService.deleteLogById(uId);
        return "redirect:/admin/accessLog";
    }

    /**
     * 文章搜索
     */
    @GetMapping("/admin/adminArticle/search")
    public String searchArticleByKey(String key, Model model) {
        if (StringUtils.isNotBlank(key)) {
            return adminService.searchArticleByKey(key, model);
        } else {
            return "redirect:/error/404";
        }
    }

    /**
     * 评论搜索
     */
    @GetMapping("/admin/comment/search")
    public String searchCommentByKey(String key, Model model) {
        if (StringUtils.isNotBlank(key)) {
            return adminService.searchCommentByKey(key, model);
        } else {
            return "redirect:/error/404";
        }
    }

    /**
     * 访问记录页,支持最近300条访问记录查看
     */
    @GetMapping("/admin/accessLog/search")
    public String searchAccessLogByKey(String key, Model model) {
        if (StringUtils.isNotBlank(key)) {
            return adminService.searchAccessLogByKey(key, model);
        } else {
            return "redirect:/error/404";
        }
    }

}
