window.addEventListener('load', function() {

    //table栏切换
    //获取元素
    var main_content_nav_list = document.querySelector('.main-content-nav ul');
    var post_lists = document.querySelectorAll('div[class^="post-list"]')
        // 给li添加自定义属性
    for (var i = 0; i < main_content_nav_list.children.length; i++) {
        main_content_nav_list.children[i].setAttribute('index', i)
    }
    //添加点击事件
    main_content_nav_list.addEventListener('click', function(e) {
        //兼容
        e = e || window.event;
        //消除全部的current
        for (let i = 0; i < main_content_nav_list.children.length; i++) {
            main_content_nav_list.children[i].children[0].className = '';
            post_lists[i].style.display = 'none';
        }
        //给点击的元素添加current
        e.target.className = 'current';
        var index = e.target.parentNode.getAttribute('index') - 0;
        post_lists[index].style.display = 'block';
    })



    // 二级菜单
    //获取元素
    var web_class_list_li = document.querySelectorAll('.web-class-list-li');
    // web_class_list.addEventListener('mouseover', function(e) {
    //     e.target.children[1].style.display = 'block'
    // })
    // web_class_list.addEventListener('mouseout', function(e) {
    //     e.target.children[1].style.display = 'none'
    // })
    for (let i = 0; i < web_class_list_li.length; i++) {
        web_class_list_li[i].addEventListener('mouseover', function() {
            this.children[1].style.display = 'block';
        })
        web_class_list_li[i].addEventListener('mouseout', function() {
            this.children[1].style.display = 'none';
        })
    }



    //关闭广告
    var close_t = document.querySelector('.close-t');
    var tenxun = document.querySelector('.tenxun');
    close_t.addEventListener('click', function() {
        tenxun.style.display = 'none';
        close_t.style.display = 'none';
    })
});