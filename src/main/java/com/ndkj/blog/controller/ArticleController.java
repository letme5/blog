package com.ndkj.blog.controller;

import com.ndkj.blog.service.impl.BlogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @program: Blog
 * @description: ArticleController
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
@Controller
public class ArticleController {

    @Autowired
    private BlogServiceImpl blogService;

    @GetMapping("article1")
    public String a1() {
        return "article";
    }

    @GetMapping("/details/{blogId}")
    public String details(@PathVariable("blogId") Integer blogId, Model model) {
        return blogService.queryArticleById(blogId, model);
    }
}
