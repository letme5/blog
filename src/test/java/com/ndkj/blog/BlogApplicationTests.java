package com.ndkj.blog;

import com.ndkj.blog.mapper.AdminMapper;
import com.ndkj.blog.mapper.BlogMapper;
import com.ndkj.blog.service.impl.AdminServiceImpl;
import com.ndkj.blog.service.impl.BlogServiceImpl;
import com.ndkj.blog.utils.MailUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
@SuppressWarnings("all")
class BlogApplicationTests {

    @Autowired
    private BlogMapper blogMapper;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    JavaMailSenderImpl mailSender;
    @Autowired
    private AdminServiceImpl adminService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private MailUtils mailUtils;

    @Test
    void contextLoads() throws MessagingException {
        mailUtils.sendMail("test", "testmail", "931936469@qq.com");
        System.out.println("2222");
    }
}
