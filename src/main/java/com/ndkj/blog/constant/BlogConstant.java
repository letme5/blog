package com.ndkj.blog.constant;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @program: Blog
 * @description: BlogConstant
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
public interface BlogConstant {

    interface NormalConstant {
        /**
         * 文章阅读数自增步长
         */
        int READ_NUM_AUTO_INCREMENT = 1;
        /**
         * 博客文章访问量自增步长
         */
        int BLOG_ACCESS_AUTO_INCREMENT = 1;
    }

    interface AdminConstant {
        /**
         * 默认admin用户名
         */
        String DEFAULT_ADMIN_NAME = "administrator";
        /**
         * 后天登录鉴权Session Key
         */
        String SESSION_KEY = "loginUser";
        /**
         * 后台访客记录展示条数
         */
        int VISITOR_SHOW_NUM = 500;
    }

    interface Attribution {
        /**
         * 归属地查询URL1
         */
        String URL = "http://whois.pconline.com.cn/ipJson.jsp?ip=";
        /**
         * 归属地查询URL2,速度比URL1稍慢，但是信息较多
         * http://ip-api.com/json/14.153.185.22?lang=zh-CN
         */
        String URL2_PREFIX = "http://ip-api.com/json/";
        String URL2_SUFFIX = "?lang=zh-CN";
    }

    /**
     * 博文状态标志位
     */
    interface BlogStatus {
        int PUBLISH = 1;
        int DELETED = 0;
    }

    interface LoginConstant {
        /**
         * 最大登录失败次数
         */
        int MAX_LOGIN_TIME = 5;
    }

}
