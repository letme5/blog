package com.ndkj.blog.constant;

/**
 * @program: Blog
 * @description: FaceConstant
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
public interface FaceConstant {

    /**
     * 人脸识别验证常量
     */
    interface FaceLogin {
        /**
         * 人脸相似度通过阈值
         */
        double FACE_SIMILAR_ACCEPT_RATE = 0.8;
        /**
         * 活性检测通过阈值
         */
        double FACE_LIVE_ACCEPT_RATE = 0.8;
    }

    /**
     * 图片传输编码格式
     */
    interface FaceEncoding {
        /**
         * 编码方式
         */
        String ENCODING_TYPE_BASE64 = "BASE64";
    }

    /**
     * login.js判断位
     */
    interface FaceCheckReturn {
        /**
         * 验证通过返回值
         */
        String SUCCESS = "success";
        /**
         * 验证失败返回值
         */
        String FAIL = "fail";
    }

    interface BaiduAiSetting {

        /**
         * BaiDuFace-TokenAuthUrl
         */
        String TokenAuthUrl = "https://aip.baidubce.com/oauth/2.0/token?";

        String FaceSearchUrl = "https://aip.baidubce.com/rest/2.0/face/v3/search";

        /**
         * 活体检测url
         */
        String FaceLiveUrl = "https://aip.baidubce.com/rest/2.0/face/v3/faceverify";
    }

}
