package com.ndkj.blog.service;

import com.ndkj.blog.pojo.entity.Blog;
import com.ndkj.blog.pojo.entity.Comment;
import com.ndkj.blog.pojo.entity.IpLog;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @program: Blog
 * @description: AdminService
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
@SuppressWarnings("all")
public interface AdminService {

    /**登录控制*/
    Integer isLoginSuccess(String adminName, String adminPwd);

    /**查询所有博客args*/
    List<String> queryArgsList();

    /**添加博客文章*/
    Integer addBlog(Blog blog);

    /**获取网站的访问总量*/
    Integer accessNum();

    /**获取mySQL版本*/
    String dbVersion();

    /**获取文章数量*/
    Integer blogsNum();

    /**查询博客args数量*/
    Integer queryArgsNum();

    /**管理员个数*/
    Integer adminNum();

    /**管理员登录次数*/
    Integer loginNum(String adminName);

    /**管理员登录次数自增*/
    Integer addLoginNum(String userName);

    /**最近一次登录时间更新*/
    Integer updateLoginTime(String adminName,Date loginTime);

    /**根据adminName查询最近一次登陆时间*/
    Date queryLoginTimeByName(String adminName);

    /**查询所有博客信息*/
    List<Blog> queryAllBlogs();

    /**根据id删除blog*/
    Integer deleteBlogById(Integer blogId);

    /**根据id查询blog*/
    Blog queryBlogById(Integer blogId);

    /**更新博客*/
    Integer updateBlog(Blog blog);

    /**根据id查询博客是否存在*/
    Integer isBlogExist(Integer blogId);

    /**查询全部评论*/
    List<Comment> queryAllComment();

    /**根据评论id删除评论*/
    Integer deleteCommentById(Integer comId);

    /**查询评论总条数*/
    Integer queryCommentsNum();

    /**查询自定义访问记录条数*/
    List<IpLog> queryLogsNum();

    /**根据uid删除日志*/
    Integer deleteLogById(Integer uId);

    /**查询出昨日一天的访问情况*/
    List<IpLog> queryYesterdayAccessLogs();

    /**查询出昨天一天的人数访问量*/
    Integer queryYesterdayAccessNum();

    /**查询出昨天一天的请求量，不去重*/
    Integer queryYesterdayAllAccessNum();

    /**查询出昨天一天的去重请求者的ip归属地*/
    List<String> queryYesterdayAccessAttribute();

    String adminIndex(String username, Model model);

    String loginControl(String userName, String userPwd, HttpSession session, Model model, HttpServletRequest request);

    String CommentList(Model model);

    String ArticleList(Model model);

    String searchArticleByKey(String key, Model model);

    String searchCommentByKey(String key, Model model);

    String searchAccessLogByKey(String key, Model model);
}
