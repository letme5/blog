package com.ndkj.blog.pojo.entity;

import com.ndkj.blog.utils.TimeFormatUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @program: Blog
 * @description:
 * @author: liuyong
 * @create: 2021-08-10 11:12
 **/
@NoArgsConstructor
@AllArgsConstructor
@Data
public class IpLog {
    public int uId;
    private Date accessDate;
    private String accessIp;
    private String ipAttribution;
    private String accessPath;

    @Override
    public String toString() {
        return "时间=" + TimeFormatUtils.TimeParse(accessDate) +
                ", url='" + accessPath + '\'' ;
    }
}
