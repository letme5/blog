package com.ndkj.blog.constant;


/**
 * @program: Blog
 * @description: MailConstant
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
public interface MailConstant {

    /**
     * 邮件发送方地址
     */
    String MAIL_SEND_ACCOUNT_ADDRESS = "lygoup@163.com";

    /**
     * 邮件接收方地址
     */
    String MAIL_RECEIVE_ADDRESS = "931936469@qq.com";

    /**
     * 博客预警地址名
     */
    String MAIL_WARNING_ACCOUNT = "博客预警中心";


}
