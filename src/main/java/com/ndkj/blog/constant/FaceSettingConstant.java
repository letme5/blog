package com.ndkj.blog.constant;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @program: blog
 * @description: 人脸识别支持配置
 * @author: liuyong
 * @create: 2022-05-22 20:46
 */
@Component
public class FaceSettingConstant {

    /**
     * BaiDuFace-ApplicationKey
     */
    @Value("${face.application-key}")
    public String applicationKey;
    /**
     * BaiDuFace-SecretKey
     */
    @Value("${face.secret-key}")
    public String secretKey;

}
