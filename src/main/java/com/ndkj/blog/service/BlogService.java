package com.ndkj.blog.service;

import com.ndkj.blog.pojo.entity.Blog;
import com.ndkj.blog.pojo.vo.BlogVo;
import com.ndkj.blog.pojo.entity.Comment;
import com.ndkj.blog.pojo.entity.IpLog;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @program: Blog
 * @description: BlogService
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
@SuppressWarnings("all")
public interface BlogService {

    String toIndex(HttpSession session);

    Blog queryBlogById(int blogId);

    /**所有博客页的简略显示*/
    List<BlogVo> queryAllBlogMiNi();

    /**模糊查询该年份所有的博客*/
    List<BlogVo> queryBlogsByYear(int year);

    /**根据博客args查询所有博客*/
    List<BlogVo> queryAllBlogMiNiByArgs(String args);

    /**根据请求的博客id让阅读量+1*/
    Integer addReadNum(int blogId);

    /**访问主页那么访问量自增*/
    Integer addAccessNum();

    /**获取网站的访问总量*/
    Integer accessNum();

    /**根据tags查询博客数量*/
    Integer queryBlogsNumByArgs(String args);

    /**根据id查询博客是否存在*/
    Integer isBlogExist(Integer blogId);

    /**根据blogID查询出对应文章的所有评论*/
    List<Comment> queryCommentsByBlogId(Integer blogId);

    /**插入评论*/
    Integer insertComment(Comment comment);

    /**添加访问记录*/
    Integer insertLog(IpLog ipLog);

    /**博客上一篇*/
    BlogVo queryLastBlog(int blogId);

    /**博客下一篇*/
    BlogVo queryNextBlog(int blogId);

    String queryArticleById(Integer blogId, Model model);

    String commentPublish(String avatarPath, Integer blogId, String replyName, String comment);

    String articalsList(Model model);

    String articalsListByLabels(Model model);
}
