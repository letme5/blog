package com.ndkj.blog.utils;

/**
 * @program: Blog
 * @description:
 * @author: liuyong
 * @create: 2021-08-09 20:15
 **/
@SuppressWarnings("all")
public class SystemUtils {
    //java版本号
    public final String java_version = System.getProperty("java.version");
    //Java提供商名称
    public final String java_vendor = System.getProperty("java.vendor");
    //Java提供商主页
    public final String java_vendor_url = System.getProperty("java.vendor.url");
    //jre目录
    public final String java_home = System.getProperty("java.home");
    //Java虚拟机规范版本号
    public final String java_vm_specification_version = System.getProperty("java.vm.specification.version");
    //Java虚拟机规范提供商
    public final String java_vm_specification_vendor = System.getProperty("java.vm.specification.vendor");
    //Java虚拟机规范名称
    public final String java_vm_specification_name = System.getProperty("java.vm.specification.name");
    //Java虚拟机版本号
    public final String java_vm_version = System.getProperty("java.vm.version");
    //Java虚拟机提供商
    public final String java_vm_vendor = System.getProperty("java.vm.vendor");
    //Java虚拟机名称
    public final String java_vm_name = System.getProperty("java.vm.name");
    //Java规范版本号
    public final String java_specification_version = System.getProperty("java.specification.version");
    //Java规范提供商
    public final String java_specification_vendor = System.getProperty("java.specification.vendor");
    //Java规范名称
    public final String java_specification_name = System.getProperty("java.specification.name");
    //Java类及lib的路径
    public final String java_class_path = System.getProperty("java.class.path");
    //系统的classpath
    public final String java_library_path = System.getProperty("java.library.path");
    //Java输入输出临时路径
    public final String java_io_tmpdir = System.getProperty("java.io.tmpdir");
    //Java编译器
    public final String java_compiler = System.getProperty("java.compiler");
    //Java执行路径
    public final String java_ext_dirs = System.getProperty("java.ext.dirs");
    //操作系统名称
    public final String os_name = System.getProperty("os.name");
    //操作系统的架构
    public final String os_arch = System.getProperty("os.arch");
    //操作系统版本号
    public final String os_version = System.getProperty("os.version");
    //目录分隔符
    public final String file_separator = System.getProperty("file.separator");
    //path分隔符
    public final String path_separator = System.getProperty("path.separator");
    //直线分隔符
    public final String line_separator = System.getProperty("line.separator");
    //操作系统用户名
    public final String user_name = System.getProperty("user.name");
    //当前用户的主目录
    public final String user_home = System.getProperty("user.home");
    //当前程序所在目录
    public final String user_dir = System.getProperty("user.dir");

    public String getJava_version() {
        return java_version;
    }

    public String getJava_vendor() {
        return java_vendor;
    }

    public String getJava_vendor_url() {
        return java_vendor_url;
    }

    public String getJava_home() {
        return java_home;
    }

    public String getJava_vm_specification_version() {
        return java_vm_specification_version;
    }

    public String getJava_vm_specification_vendor() {
        return java_vm_specification_vendor;
    }

    public String getJava_vm_specification_name() {
        return java_vm_specification_name;
    }

    public String getJava_vm_version() {
        return java_vm_version;
    }

    public String getJava_vm_vendor() {
        return java_vm_vendor;
    }

    public String getJava_vm_name() {
        return java_vm_name;
    }

    public String getJava_specification_version() {
        return java_specification_version;
    }

    public String getJava_specification_vendor() {
        return java_specification_vendor;
    }

    public String getJava_specification_name() {
        return java_specification_name;
    }

    public String getJava_class_path() {
        return java_class_path;
    }

    public String getJava_library_path() {
        return java_library_path;
    }

    public String getJava_io_tmpdir() {
        return java_io_tmpdir;
    }

    public String getJava_compiler() {
        return java_compiler;
    }

    public String getJava_ext_dirs() {
        return java_ext_dirs;
    }

    public String getOs_name() {
        return os_name;
    }

    public String getOs_arch() {
        return os_arch;
    }

    public String getOs_version() {
        return os_version;
    }

    public String getFile_separator() {
        return file_separator;
    }

    public String getPath_separator() {
        return path_separator;
    }

    public String getLine_separator() {
        return line_separator;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_home() {
        return user_home;
    }

    public String getUser_dir() {
        return user_dir;
    }
}
