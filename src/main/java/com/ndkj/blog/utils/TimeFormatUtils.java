package com.ndkj.blog.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: SpringBoot
 * @description:
 * @author: liuyong
 * @create: 2021-07-24 19:35
 **/
public class TimeFormatUtils {

    /**日期转换*/
    public static String TimeParse(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }

}
