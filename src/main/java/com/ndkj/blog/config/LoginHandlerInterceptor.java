package com.ndkj.blog.config;

import com.ndkj.blog.constant.BlogConstant;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @program: Blog
 * @description: 自定义请求拦截器，拦截后台localhost:8080/admin/*，防止用户未登录直接访问登陆成功后的页面
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //登陆成功之后，应该有用户的session
        Object loginUser = request.getSession().getAttribute(BlogConstant.AdminConstant.SESSION_KEY);
        //没有登录,跳转到登录页面
        if (loginUser == null) {
            request.setAttribute("msg", "没有权限，请先登录");
            request.getRequestDispatcher("/admin").forward(request, response);
            return false;
        } else {
            return true;
        }
    }

}
