package com.ndkj.blog.service.impl;

import com.ndkj.blog.constant.FaceConstant;
import com.ndkj.blog.pojo.bo.FaceVerifyBO;
import com.ndkj.blog.utils.GetToken;
import com.ndkj.blog.utils.GsonUtils;
import com.ndkj.blog.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @program: Face-recognition-login-master
 * @description: 在线图片活体v3
 * @author: liuyong
 * @create: 2022-03-11 18:23
 */
@Slf4j
@Component
public class FaceLiveVerify {

    @Autowired
    private GetToken getToken;

    /**
     * 重要提示代码中所需工具类
     * FileUtil,Base64Util,HttpUtil,GsonUtils请从
     * https://ai.baidu.com/file/658A35ABAB2D404FBF903F64D47C1F72
     * https://ai.baidu.com/file/C8D81F3301E24D2892968F09AE1AD6E2
     * https://ai.baidu.com/file/544D677F5D4E4F17B4122FBD60DB82B3
     * https://ai.baidu.com/file/470B3ACCA3FE43788B5A963BF0B625F3
     * 下载
     */
    public String faceVerify(List<FaceVerifyBO> faceVerifyBOS) {
        // 请求url
        String url = FaceConstant.BaiduAiSetting.FaceLiveUrl;
        try {
            String param = GsonUtils.toJson(faceVerifyBOS);
            // 注意这里仅为了简化编码每一次请求都去获取access_token，线上环境access_token有过期时间， 客户端可自行缓存，过期后重新获取。
            String accessToken = getToken.getAuth();
            String result = HttpUtils.post(url, accessToken, "application/json", param);
            System.out.println("活体检测url请求结果：" + result.substring(0, result.length() > 150 ? 150 : result.length()) + "...");
            return result;
        } catch (Exception e) {
            log.error("活体检测服务端异常");
            e.printStackTrace();
        }
        return null;
    }
}
