package com.ndkj.blog.constant;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.thymeleaf.expression.Arrays;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @program: blog
 * @description: 博客后台登录安全归属地，从nacos中热更新获取
 * @author: liuyong
 * @create: 2022-05-07 20:34
 */
@RefreshScope(proxyMode = ScopedProxyMode.DEFAULT)
@Configuration
@Scope(value = "prototype")
@Slf4j(topic = "com.safe.locations")
public class SafeLocation {

    /**
     * 管理员常用登录地,该属性仅为启动时获取，类中获取请使用下面的get方法
     */
    @Value("${safe.locations}")
    public List<String> COMMONLY_USED_ATTRIBUTION;

    public List<String> getCommonlyUsedAttribution() {
        if (CollectionUtils.isEmpty(COMMONLY_USED_ATTRIBUTION)) {
            ArrayList<String> defaultList = new ArrayList<>();
            defaultList.add("江西");
            defaultList.add("局域网");
            defaultList.add("广东");
            log.error("从nacos获取安全登陆地失败，使用默认登陆地集合{}", defaultList);
            return defaultList;
        }
        return COMMONLY_USED_ATTRIBUTION;
    }
}
