package com.ndkj.blog.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @program: SpringBoot
 * @description:
 * @author: liuyong
 * @create: 2021-07-24 12:43
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog {
    private Integer blogId;
    private Date putTime;
    private Integer readNum;
    private String title;
    private String content;
    private String args;
    private Integer status;
}
