package com.ndkj.blog.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: SpringBoot
 * @description:
 * @author: liuyong
 * @create: 2021-07-24 16:42
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlogVo implements Serializable {
    public String blogId;
    private Date putTime;
    private String title;
    private String args;
}
