package com.ndkj.blog.handler;

import io.lettuce.core.RedisConnectionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @program: blog
 * @description: 自定义异常处理器
 * @author: liuyong
 * @create: 2022-04-29 17:10
 */
@RestControllerAdvice
@Slf4j(topic = "blog.GlobalExceptionHandler")
public class CustomizeExceptionHandler {

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public void httpRequestMethodNotSupportedExceptionHandler() {}

    @ExceptionHandler(value = {RedisConnectionFailureException.class, RedisConnectionException.class})
    public String redisConnectionExceptionHandler() {
        log.error("redis connection failed");
        return "redis connection failed";
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public void illegalArgumentExceptionHandler() {}

}
