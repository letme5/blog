package com.ndkj.blog.timing;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @program: blog
 * @description: Redis心跳检测
 * @author: liuyong
 * @create: 2022-05-05 10:32
 */
@Slf4j(topic = "com.blog.redis.redisStateCheckTask")
public class RedisStateCheck {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 每30分钟检测一次redis状态
     */
    @Scheduled(cron = "0 0/30 * * * ?")
    public void stateCheckTask() {
        try {
            redisTemplate.getConnectionFactory().getConnection();
            log.info("[定时任务]redis服务连接正常");
        } catch (Exception e) {
            log.error("无法连接到Redis服务器:redisTemplate.getConnectionFactory().getConnection() error");
        }
    }

}
