# Personal Blog System

#### 介绍
基于ThymeLeaf/SpringBoot/Nacos/Redis/Mybatis/MySQL的个人博客系统。

演示地址：`https://www.liuyo.top`,后台地址：`https://www.liuyo.top/admin`

如果对您有帮助，敬请点个Star~

#### 功能说明
    1.响应式设计，适应手机、平板、电脑等各型UI
    2.壁纸级星空动态背景(网页F11全屏)
    3.整合UEditor实现自由的文章编辑
    4.支持主题切换
    5.支持其余博客系统所具备的基本功能
    6.支持人脸识别登录
    7.支持登录归属地异常告警
    8.支持登录失败阈值告警
    9.支持热部署、集群部署
    10.支持邮箱收取博客运行信息
    11.支持访问者归属地查询

#### 欢迎赞助
为维护博客系统昂贵的服务器支出，博客系统开设打赏版块，支持开源，为爱发电~

<img src="https://gitee.com/letme5/blog/raw/master/src/main/resources/static/images/pay/wechat.png" alt="img" width="40%" height="50%" />

<img src="https://gitee.com/letme5/blog/raw/master/src/main/resources/static/images/pay/zhifubao.jpg" alt="img" width="40%" height="50%" />

#### 使用前说明
博客系统始于大三开始编写。 

由于作者水平有限，在代码编写之初存在部分不合理的代码设计，后续对源码进行了数次迭代，由于平时工作学习较忙，仍有部分代码来不及进行重构，请君包涵。

博客系统需要你的加入，欢迎在Gitee上为本项目提交Pr，如果设计合理我会通过Merge的,感谢您为博客系统的贡献。

打赏时请备注昵称，您的名字将会被展示在博客系统技术支持页面（提交pr也可以）。

最后，记得点个Star~

#### 安装教程
##### 环境准备
> 1.Nacos(最好1.3.0版本，如未安装可在网上查找安装教程)
> 
> 2.Redis服务器
> 
> 3.百度人脸识别服务ApplicationKey、SecretKey，申请地址（免费）：https://login.bce.baidu.com/?redirect=https%3A%2F%2Fconsole.bce.baidu.com%2F%3Ffromai%3D1#/aip/overview
> 
> 4.MySQL(我这里采用的8.0+版本，其余版本也可)
> 
> 5.JDK1.8

##### 开始搭建
> 1. 拷贝源代码至IDEA
> 
> 
> 2. 打开application.yaml，选中全部，按`Ctrl+/`批量取消行首注释
> 
> 
> 4. 取消注释后，按照配置上方的注释填写配置项，将配置好的全部内容复制到Nacos配置列表中（可新建一个Dev命名空间存放配置）
> 
> 
> 6. 再将application.yaml中的全部配置注释（用nacos中的配置即可）
> 
> 
> 8. 打开bootstrap.yaml，按照你此时运行的nacos环境填写其余配置项
> 
> 
> 10. 新建数据库blog
> 
> 
> 12. 新建`admin`表：
> 
>          CREATE TABLE `admin` 
> 
>          `id` bigint unsigned NOT NULL AUTO_INCREMENT,
>     
>          `adminName` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
>     
>          `adminPwd` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
>     
>          `loginNum` int DEFAULT NULL,
>     
>          `lastLoginTime` datetime DEFAULT NULL,
>     
>          `status` tinyint(1) NOT NULL DEFAULT '1',
>     
>          PRIMARY KEY (`id`))
> 
>          ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
> 
> 13. 新建数据库表`blog_accesslog`:
> 
>           CREATE TABLE `blog_accesslog` (
>    
>          `uId` bigint unsigned NOT NULL AUTO_INCREMENT,
>   
>          `accessDate` datetime NOT NULL,
>   
>          `accessIp` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
>   
>          `ipAttribution` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
>   
>          `accessPath` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
>   
>          PRIMARY KEY (`uId`) USING BTREE)
>   
>          ENGINE=InnoDB AUTO_INCREMENT=819835 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
> 
> 14. 新建数据库表`blog_comment`:
> 
>          CREATE TABLE `blog_comment` (
>
>          `comId` bigint NOT NULL AUTO_INCREMENT,
> 
>          `comTime` datetime NOT NULL,
> 
>          `comment` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
> 
>          `comName` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
> 
>          `blogId` bigint NOT NULL,
> 
>          `avatarPath` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
> 
>          PRIMARY KEY (`comId`))
> 
>          ENGINE=InnoDB AUTO_INCREMENT=8175 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
> 
> 15. 新建数据库表`blog_detail`:
> 
>         CREATE TABLE `blog_detail` (
> 
>         `blogId` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '博客文章主键id',
> 
>         `putTime` datetime NOT NULL COMMENT '博客提交时间',
> 
>         `readNum` bigint NOT NULL COMMENT '博客被阅次数',
> 
>         `title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '博客标题',
> 
>         `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '博客内容',
> 
>         `args` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '标签',
> 
>         `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0隐藏',
> 
>         PRIMARY KEY (`blogId`))
> 
>         ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
> 
> 16. 新建数据库表`blog_info`:
> 
>         CREATE TABLE `blog_info` (
> 
>         `accessNum` int NOT NULL,
> 
>         PRIMARY KEY (`accessNum`))
> 
>         ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
> 
> 17. 运行项目

#### 使用说明

如果遇到无法解决的困难，作者提供技术支持，联系方式：

QQ：`931936469`

邮箱：`lygoup@163.com`

`作者需要花时间进行处理，请打赏之后联系作者时备注您的打赏昵称。`

#### 欢迎赞助


<img src="https://gitee.com/letme5/blog/raw/master/src/main/resources/static/images/pay/wechat.png" alt="img" width="40%" height="50%" />

<img src="https://gitee.com/letme5/blog/raw/master/src/main/resources/static/images/pay/zhifubao.jpg" alt="img" width="40%" height="50%" />

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 最后
博客系统仅用于学习交流，未经许可，请勿用于商业用途。

感谢`brave_Y` & `leopardpan`为本站开发提供帮助。 


