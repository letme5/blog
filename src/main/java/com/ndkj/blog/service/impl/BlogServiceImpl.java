package com.ndkj.blog.service.impl;

import com.ndkj.blog.constant.BlogConstant;
import com.ndkj.blog.constant.MailConstant;
import com.ndkj.blog.mapper.BlogMapper;
import com.ndkj.blog.pojo.entity.Blog;
import com.ndkj.blog.pojo.vo.BlogVo;
import com.ndkj.blog.pojo.entity.Comment;
import com.ndkj.blog.pojo.entity.IpLog;
import com.ndkj.blog.service.BlogService;
import com.ndkj.blog.utils.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    @Qualifier("blogMapper")
    private BlogMapper blogMapper;
    @Autowired
    private MailUtils mailUtils;

    Set<String> argsSet = new HashSet<>();

    @Override
    public String toIndex(HttpSession session) {
        // 回到首页即会清除session
        if (session.getAttribute(BlogConstant.AdminConstant.SESSION_KEY) != null) {
            session.removeAttribute(BlogConstant.AdminConstant.SESSION_KEY);
        }
        return "index";
    }

    @Override
    public Blog queryBlogById(int blogId) {
        return blogMapper.queryBlogById(blogId);
    }

    @Override
    public List<BlogVo> queryAllBlogMiNi() {
        return blogMapper.queryAllBlogMiNi();
    }

    @Override
    public List<BlogVo> queryBlogsByYear(int year) {
        return blogMapper.queryBlogsByYear(year);
    }

    @Override
    public List<BlogVo> queryAllBlogMiNiByArgs(String args) {
        return blogMapper.queryAllBlogMiNiByArgs(args);
    }

    @Override
    public Integer addReadNum(int blogId) {
        return blogMapper.addReadNum(blogId);
    }

    @Override
    public Integer addAccessNum() {
        return blogMapper.addAccessNum();
    }

    @Override
    public Integer accessNum() {
        return blogMapper.accessNum();
    }

    @Override
    public Integer queryBlogsNumByArgs(String args) {
        return blogMapper.queryBlogsNumByArgs(args);
    }

    @Override
    public Integer isBlogExist(Integer blogId) {
        return blogMapper.isBlogExist(blogId);
    }

    @Override
    public List<Comment> queryCommentsByBlogId(Integer blogId) {
        return blogMapper.queryCommentsByBlogId(blogId);
    }

    @Override
    public Integer insertComment(Comment comment) {
        return blogMapper.insertComment(comment);
    }

    @Override
    public Integer insertLog(IpLog ipLog) {
        return blogMapper.insertLog(ipLog);
    }

    @Override
    public BlogVo queryLastBlog(int blogId) {
        return blogMapper.queryLastBlog(blogId);
    }

    @Override
    public BlogVo queryNextBlog(int blogId) {
        return blogMapper.queryNextBlog(blogId);
    }

    @Override
    public String queryArticleById(Integer blogId, Model model) {
        if (this.isBlogExist(blogId) == 1) {
            this.addReadNum(blogId);
            this.addAccessNum();
            BlogVo lastBlog = this.queryLastBlog(blogId);
            BlogVo nextBlog = this.queryNextBlog(blogId);
            Blog blog = this.queryBlogById(blogId);
            model.addAttribute("lastBlog", lastBlog);
            model.addAttribute("nextBlog", nextBlog);
            model.addAttribute("blogId", blogId);
            model.addAttribute("blogTime", blog.getPutTime());
            model.addAttribute("readNum", blog.getReadNum());
            model.addAttribute("detail", blog.getContent());
            model.addAttribute("title", blog.getTitle());
            model.addAttribute("commentsList", this.queryCommentsByBlogId(blogId));
            return "pages/article";
        } else {
            return "redirect:error/404";
        }
    }

    @Override
    public String commentPublish(String avatarPath, Integer blogId, String replyName, String comment) {
        if (this.isBlogExist(blogId) == 1) {
            this.insertComment(new Comment(1, new Date(), comment, replyName, blogId, avatarPath));
            mailUtils.sendMail("您收到了新的评论", "评论内容：" + comment + "</br>评论文章：" +
                    this.blogMapper.queryBlogById(blogId).getTitle() + "</br>昵称：" + replyName, MailConstant.MAIL_RECEIVE_ADDRESS);
        }
        return "redirect:/details/" + blogId;
    }

    @Override
    public String articalsList(Model model) {
        model.addAttribute("accessNum", this.accessNum());
        //检索出各年份的博客数据
        model.addAttribute("blogs2022", this.queryBlogsByYear(2022));
        model.addAttribute("blogs2021", this.queryBlogsByYear(2021));
        model.addAttribute("blogs2020", this.queryBlogsByYear(2020));
        return "pages/all-article";
    }

    @Override
    public String articalsListByLabels(Model model) {
        List<BlogVo> blogVos = this.queryAllBlogMiNi();
        // hashSet去重复
        for (BlogVo blogVo : blogVos) {
            argsSet.add(blogVo.getArgs());
        }
        model.addAttribute("blogService", this);
        model.addAttribute("argsSet", argsSet);
        model.addAttribute("accessNum", this.accessNum());
        return "pages/args";
    }

}
