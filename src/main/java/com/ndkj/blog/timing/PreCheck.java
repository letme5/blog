package com.ndkj.blog.timing;

import com.ndkj.blog.constant.SafeLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @program: blog
 * @description: 检查系统连接redis状态
 * @author: liuyong
 * @create: 2022-05-04 14:49
 */
@Component
@Slf4j(topic = "com.blog.preCheck")
public class PreCheck implements ApplicationRunner {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private SafeLocation safeLocation;
    @Autowired
    private Environment environment;

    /**
     * Callback used to run the bean.
     * use @PostConstructor or CommandLineRunner can accomplish the same function.
     *
     * @param args incoming application arguments
     * @throws Exception on error
     */
    @Override
    public void run(ApplicationArguments args) {
        log.info("nacos->safe locations:{}", safeLocation.COMMONLY_USED_ATTRIBUTION);
        log.info("nacos->datasource url:{}", environment.getProperty("spring.datasource.url"));
        log.info("nacos->redis host:{}", environment.getProperty("spring.redis.host"));
        try {
            RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
            log.info("已正常连接redis服务器:{}", connection);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("无法连接Redis:redisTemplate.getConnectionFactory().getConnection() error");
        }
    }

}
