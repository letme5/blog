package com.ndkj.blog.controller;

import com.ndkj.blog.service.impl.BlogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @program: SpringBoot
 * @description:
 * @author: liuyong
 * @create: 2021-08-01 20:20
 **/
@Controller
public class CommentController {

    @Autowired
    private BlogServiceImpl blogService;

    @PostMapping("/details/reply")
    public String reply(@RequestParam("avatarPath") String avatarPath, @RequestParam("blogId") Integer blogId,
                        @RequestParam("replyName") String replyName, @RequestParam("comment") String comment) {
        return blogService.commentPublish(avatarPath, blogId, replyName, comment);
    }

}
