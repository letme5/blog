package com.ndkj.blog.controller;

import com.ndkj.blog.service.impl.BlogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

/**
 * @program: SpringBoot
 * @description: IndexController
 * @author: liuyong
 * @create: 2021-07-23 16:39
 **/
@Controller
public class IndexController {

    @Autowired
    private BlogServiceImpl blogService;

    @GetMapping("/index")
    public String toIndex(HttpSession session) {
        return blogService.toIndex(session);
    }

    @GetMapping("/index2")
    @Deprecated
    public String toIndexDetails() {
        return "pages/index-new";
    }

    @GetMapping("allArticle")
    public String allArticle(Model model) {
        return blogService.articalsList(model);
    }

    @GetMapping("label")
    public String label(Model model) {
        return blogService.articalsListByLabels(model);
    }

    @GetMapping("TechSupport")
    public String techSupport(Model model) {
        model.addAttribute("accessNum", blogService.accessNum());
        return "pages/support";
    }

    @GetMapping("aboutme")
    public String aboutme(Model model) {
        model.addAttribute("accessNum", blogService.accessNum());
        return "pages/aboutme";
    }
}
