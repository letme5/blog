package com.ndkj.blog.constant;

/**
 * @program: Blog
 * @description: RedisConstant
 * @author: liuyong
 * @create: 2021-09-23 17:24
 */
public interface CacheConstant {

    interface LoginIpTimes {
        /**
         * redis key前缀
         */
        String PREFIX = "login:user:";
        /**
         * redis key的存活时间,MINUTES
         */
        int TTL = 4;
    }

}
