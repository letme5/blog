package com.ndkj.blog.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Objects;

/**
 * @program: SpringBoot
 * @description:
 * @author: liuyong
 * @create: 2021-07-28 17:42
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Administrator {
    private Integer id;
    private String adminName;
    private String adminPwd;
    private Integer loginNum;
    private Date lastLoginTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Administrator that = (Administrator) o;
        return adminName.equals(that.adminName) && adminPwd.equals(that.adminPwd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adminName, adminPwd);
    }
}
